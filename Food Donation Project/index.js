var firebaseConfig = {
    apiKey: "AIzaSyD3HkKHZBVQC9qNoFlYpkRFb0vrPsnMgho",
    authDomain: "food-donation-ae150.firebaseapp.com",
    projectId: "food-donation-ae150",
    storageBucket: "food-donation-ae150.appspot.com",
    messagingSenderId: "776746091169",
    appId: "1:776746091169:web:4b5ca6f877b299202ee6e0",
    measurementId: "G-FDCRYE48HK"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
 
  $("#btn-signIn").click(function(){

       var email=$("#email").val();
	   var password=$("#password").val();
	   
	   if(email!="" && password!=""){
           var result=firebase.auth().signInWithEmailAndPassword(email,password);

		   result.catch(function(error){
                var errorCode=error.code;
				var errorMessage=error.Message;

				console.log(errorCode);
				console.log(errorMessage);
				window.alert("Please enter correct password.!");
		   });
	   }
	   else{
		   window.alert("Please fill out the form!");
	   }

  });

  $("#btn-signup").click(function(){

	var email=$("#email").val();
	var password=$("#password").val();
	var cpassword=$("#cpassword").val();
	
	if(email!="" && password!="" && cpassword!=""){
		var result=firebase.auth().createUserWithEmailAndPassword(email,password);

		result.catch(function(error){
			 var errorCode=error.code;
			 var errorMessage=error.Message;

			 console.log(errorCode);
			 console.log(errorMessage);
			 window.alert("Registered Successfully!");
		});
	}
	else{
		window.alert("Please fill out the form!");
	}

});


$("#btn-resetPassword").click(function(){
	var auth=firebase.auth();
	var email=$("#email").val();

	if(email!=""){
        auth.sendPasswordResetEmail(email).then(function()
		{
			window.alert("Email has sent to your emai!");
		})
		.catch(function() 
		{
			 var errorCode=error.code;
			 var errorMessage=error.Message;

			 console.log(errorCode);
			 console.log(errorMessage);
			 window.alert("Please enter correct email address!!");
		});
	}
	else{
		window.error("Please fill out the form!");
	}
});